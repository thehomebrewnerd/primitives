{
    "id": "d2fa8df2-6517-3c26-bafc-87b701c4043a",
    "version": "1.2.2",
    "name": "simon",
    "keywords": [
        "Data Type Predictor",
        "Semantic Classification",
        "Text",
        "NLP",
        "Tabular"
    ],
    "source": {
        "name": "Distil",
        "contact": "mailto:jeffrey.gleason@yonder.co",
        "uris": [
            "https://github.com/NewKnowledge/simon-d3m-wrapper"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/NewKnowledge/simon-d3m-wrapper.git@9e3197c9f47d781fec18fbee710cc06334ddc792#egg=SimonD3MWrapper"
        },
        {
            "type": "TGZ",
            "key": "simon_models_1",
            "file_uri": "http://public.datadrivendiscovery.org/simon_models_1.tar.gz",
            "file_digest": "d071106b823ab1168879651811dd03b829ab0728ba7622785bb5d3541496c45f"
        }
    ],
    "python_path": "d3m.primitives.data_cleaning.column_type_profiler.Simon",
    "algorithm_types": [
        "CONVOLUTIONAL_NEURAL_NETWORK"
    ],
    "primitive_family": "DATA_CLEANING",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "SimonD3MWrapper.wrapper.simon",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "SimonD3MWrapper.wrapper.Params",
            "Hyperparams": "SimonD3MWrapper.wrapper.Hyperparams"
        },
        "interfaces_version": "2019.11.10",
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "overwrite": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to overwrite manual annotations with SIMON annotations"
            },
            "statistical_classification": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "whether to append categorical / ordinal annotations using rule-based classification"
            },
            "multi_label_classification": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "whether to perfrom multi-label classification and append multiple annotations to metadata"
            },
            "max_rows": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 500,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "maximum number of rows to consider when classifying data type of specific column",
                "lower": 100,
                "upper": 2000,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "max_chars": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 20,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "maximum number of characters to consider when processing row",
                "lower": 1,
                "upper": 100,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "p_threshold": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.5,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "probability threshold to use when decoding classification results. \n            Predictions above p_threshold will be returned",
                "lower": 0,
                "upper": 1.0,
                "lower_inclusive": true,
                "upper_inclusive": true
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "SimonD3MWrapper.wrapper.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "volumes": {
                "type": "typing.Union[NoneType, typing.Dict[str, str]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "SimonD3MWrapper.wrapper.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "volumes"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Learns column annotations using training data. Saves to apply to testing data.\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nReturns:\n    CallResult[None]\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "SimonD3MWrapper.wrapper.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Add SIMON annotations if manual annotations do not exist. Hyperparameter overwrite controls\nwhether SIMON annotations should overwrite manual annotations or merely augment them\n\nArguments:\n    inputs {Inputs} -- full D3M dataframe, containing attributes, key, and target\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: if primitive not fit\n\nReturns:\n    CallResult[Outputs] -- Input pd frame with metadata augmented and optionally overwritten\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "produce_metafeatures": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's best guess for the structural type of each input column.\n\nArguments:\n    inputs {Inputs} -- full D3M dataframe, containing attributes, key, and target\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: if primitive not fit\n\nReturns:\n    CallResult[Outputs] -- dataframe with two columns: \"semantic type classifications\" and \"probabilities\"\n        Each row represents a column in the original dataframe. The column \"semantic type \n        classifications\" contains a list of all semantic type labels and the column\n        \"probabilities\" contains a list of the model's confidence in assigning each \n        respective semantic type label  "
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "NoneType",
                "description": "Sets primitive's training data\n\nArguments:\n    inputs {Inputs} -- D3M dataframe\n\nParameters\n----------\ninputs : Inputs\n    The inputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {}
    },
    "structural_type": "SimonD3MWrapper.wrapper.simon",
    "description": "The primitive uses a LSTM-FCN neural network trained on 18 different semantic types to infer the semantic\ntype of each column. The primitive's annotations will overwrite the default annotations if 'overwrite'\nis set to True (column roles, e.g. Attribute, PrimaryKey, Target from original annotations will be kept).\nOtherwise the primitive will augment the existing annotations with its predicted labels.\nThe primitive will append multiple annotations if multi_label_classification is set to 'True'.\nFinally, a different mode of typing inference that uses rule-based heuristics will be used  if\n'statistical_classification' is set to True.\n\nArguments:\n    hyperparams {Hyperparams} -- D3M Hyperparameter object\n\nKeyword Arguments:\n    random_seed {int} -- random seed (default: {0})\n    volumes {Dict[str, str]} -- large file dictionary containing model weights (default: {None})\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "b100285f9dce7bb5dcc285c929355a7659e14a70d35b035c1d98c3191d859ee3"
}
